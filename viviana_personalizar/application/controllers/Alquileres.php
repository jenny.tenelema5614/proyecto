<?php
class Alquileres extends CI_Controller{
  //constructor de la clase
  public function __construct(){
    parent::__construct();
    //cargando modelo cliente
    $this->load->model('cliente');
    $this->load->model('pelicula');
    $this->load->model('alquiler');
    //verificar si existe o no alguien conectado
    if (!$this->session->userdata("usuarioC0nectado")) {
      // code...
      $this->session->set_flashdata("error","Por favor ingrese al sistema");
      redirect('seguridades/cerrarSesion');
    }else { //Codigo cuando si esta conectado
      if (!($this->session->userdata("usuarioC0nectado")["perfil"]=="ADMINISTRADOR"
          || $this->session->userdata("usuarioC0nectado")["perfil"]=="VENDEDOR")) {
        redirect('seguridades/cerrarSesion');
      }
    }
  }
  public function formulario(){
    $data['listadoClientes']=$this->cliente->obtenerTodos();
    $data['listadoPeliculas']=$this->pelicula->obtenerTodos();
    $this->load->view('encabezado');
    $this->load->view('alquileres/formulario',$data);
    $this->load->view('pie');
  }
  //Funcion para insertar nuevos ALQUILERES
  public function insertarAlquiler(){

    $query = $this->db->get_where('pelicula', array('id_pel' => $this->input->post("fk_id_pel")));
    foreach ($query->result() as $row)
    {
      $val= $row->costo_alquiler_pel;
    }
    $dataAlquiler=array(
      "fk_id_cli"=>$this->input->post("fk_id_cli"),
      "fk_id_pel"=>$this->input->post("fk_id_pel"),
      "precio_alqui"=>$val,
      "fecha_inicio_alqui"=>$this->input->post("fecha_inicio_alqui"),
      "fecha_fin_alqui"=>$this->input->post("fecha_fin_alqui")
    );
    if ($this->alquiler->insertar($dataAlquiler)) {
      $this->session->set_flashdata('confirmacion','Alquiler registradoexitosamente');
      redirect('/alquileres/index');

    } else {
      // code...
      $this->session->set_flashdata('confirmacion','Error al procesar, INTENTE NUEVAMENTE');
      redirect('/alquileres/index');
    }

  }
  public function index()
  {
    $data["listadoAlquileres"]=$this->alquiler->obtenerTodos();
    //Cargando la vista index
    //carpeta/archivo
    $this->load->view('encabezado');
    $this->load->view('alquileres/index',$data);//pasando parametros a la vista
    $this->load->view('pie');
  }

  public function eliminarAlquiler($id){
    //validando si la eliminacion se realiza o no
    if ($this->alquiler->eliminarPorId($id)) {
      $this->session->set_flashdata("confirmacion","Alquiler eliminado exitosamente");
      redirect('alquileres/index');
    }else {
      echo 'Error al eliminar';
    }
  }
  public function editar($id){
    $data['alquilerEditar']=$this->alquiler->obtenerPorId($id);
    $data['listadoClientes']=$this->cliente->obtenerTodos();
    $data['listadoPeliculas']=$this->pelicula->obtenerTodos();
    //Cargando la vista index
    //carpeta/archivo
    $this->load->view('encabezado');
    $this->load->view('alquileres/editar',$data);
    $this->load->view('pie');
  }
//metodo para llamar a la actualizacion del modelo
public function actualizarAlquiler(){
  $id_alqui=$this->input->post('id_alqui');//captura el id del cliente a EDITAR
  $datosEditados=array(
    "fk_id_cli"=>$this->input->post("fk_id_cli"),
    "fk_id_pel"=>$this->input->post("fk_id_pel"),
    "precio_alqui"=>$this->input->post("costo_alquiler_pel"),
    "fecha_inicio_alqui"=>$this->input->post("fecha_inicio_alqui"),
    "fecha_fin_alqui"=>$this->input->post("fecha_fin_alqui"),
    "fecha_actualizacion_alqui"=>date('Y-m-d H:i:s')
  );
  if($this->alquiler->actualizar($id_alqui,$datosEditados)){
    $this->session->set_flashdata("confirmacion","Alquiler actualizado exitosamente");
    redirect('alquileres/index');
  }else{
    echo "Error al actualizar";
  }
}
}
