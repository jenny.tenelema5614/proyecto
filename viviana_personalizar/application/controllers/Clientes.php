<?php
class Clientes extends CI_Controller{
  //constructor de la clase
  public function __construct(){
    parent::__construct();
    //cargando modelo cliente
    $this->load->model('cliente');
    //verificar si existe o no alguien conectado
    if (!$this->session->userdata("usuarioC0nectado")) {
      // code...
      $this->session->set_flashdata("error","Por favor ingrese al sistema");
      redirect('seguridades/cerrarSesion');
    }else { //Codigo cuando si esta conectado
      if (!($this->session->userdata("usuarioC0nectado")["perfil"]=="ADMINISTRADOR"
          || $this->session->userdata("usuarioC0nectado")["perfil"]=="VENDEDOR")) {
        redirect('seguridades/cerrarSesion');
      }
    }
  }
/*Funcion que renderiza el listado
de cloientes*/
  public function index()
  {
    //Cargando la vista index
    //carpeta/archivo
    $this->load->view('encabezado');
    $this->load->view('clientes/index');
    $this->load->view('pie');
  }
  public function tablaClientes()
  {
    $idUsuarioConectado=$this->session->userdata("usuarioC0nectado")['id'];
    $data["listadoClientes"]=$this->cliente->obtenerTodosPorIdUsuario($idUsuarioConectado);
    $this->load->view('clientes/tablaClientes',$data);//pasando parametros a la vista
  }
  public function nuevo(){
    //Cargando la vista nuevo
    //carpeta/archivo
    $this->load->view('encabezado');
    $this->load->view('clientes/nuevo');
    $this->load->view('pie');
  }
  public function guardarCliente(){

    //capturar valores de la vista
      $cedula=$this->input->post('cedula_cli');
      $nombres=$this->input->post('nombres_cli');
      $apellidos=$this->input->post('apellidos_cli');
      $direccion=$this->input->post('direccion_cli');
      $telefono_convencional=$this->input->post('telefono_convencional_cli');
      $telefono_celular=$this->input->post('telefono_celular_cli');
      /*echo $email;
      echo "<br>";
      echo $password;*/
      //armando arreglo para insertar datos en la BDD
      $datosNuevoCliente=array(
        "cedula_cli"=>$cedula,
        "nombres_cli"=>$nombres,
        "apellidos_cli"=>$apellidos,
        "direccion_cli"=>$direccion,
        "telefono_convencional_cli"=>$telefono_convencional,
        "telefono_celular_cli"=>$telefono_celular,
        "fk_id_usu"=>$this->session->userdata("usuarioC0nectado")['id']
      );
      if ($this->cliente->insertar($datosNuevoCliente)) {
        //si es verdadero si se inserto
        //mensaje flash para confiramar inserccion de cliente
        $this->session->set_flashdata("confirmacion","Cliente registrado exitosamente");
        redirect('clientes/index');
      }else {
        //si es falso no se inserto
        echo "Cliente no guardado";
      }
  }
  //metodo para eliminar cliente recibiendo como parametro su id
  public function eliminarCliente($id){
    //validando si la eliminacion se realiza o no
    if ($this->cliente->eliminarPorId($id)) {
      $this->session->set_flashdata("confirmacion","Cliente eliminado exitosamente");
      redirect('clientes/index');
    }else {
      echo 'Error al eliminar';
    }
  }
  //renderizar el formulario de actualizacion
  public function editar($id){
    $data['clienteEditar']=$this->cliente->obtenerPorId($id);
    //Cargando la vista index
    //carpeta/archivo
    $this->load->view('encabezado');
    $this->load->view('clientes/editar',$data);
    $this->load->view('pie');
  }
  //metodo para llamar a la actualizacion del modelo
  public function actualizarCliente(){
    $id_cli=$this->input->post('id_cli');//captura el id del cliente a EDITAR
    $datosEditados=array(
      "cedula_cli"=>$this->input->post('cedula_cli'),
      "nombres_cli"=>$this->input->post('nombres_cli'),
      "apellidos_cli"=>$this->input->post('apellidos_cli'),
      "direccion_cli"=>$this->input->post('direccion_cli'),
      "telefono_convencional_cli"=>$this->input->post('telefono_convencional_cli'),
      "telefono_celular_cli"=>$this->input->post('telefono_celular_cli'),
      "fecha_actualizacion_cli"=>date('Y-m-d H:i:s')
    );
    if($this->cliente->actualizar($id_cli,$datosEditados)){
      $this->session->set_flashdata("confirmacion","Cliente actualizado exitosamente");
      redirect('clientes/index');
    }else{
      echo "Error al actualizar";
    }
  }

  public function validarCedulaExistente(){
          $cedula_cli=$this->input->post('cedula_cli');
         $clienteExistente=$this->cliente->consultarClientePorCedula($cedula_cli);
         if($clienteExistente){
           echo json_encode(FALSE);
         }else{
           echo json_encode(TRUE);
         }
     }

  }
?>
