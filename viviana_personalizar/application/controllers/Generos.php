<?php
class Generos extends CI_Controller{
  //constructor de la clase
  public function __construct(){
    parent::__construct();
    //cargando modelo cliente
  //  $this->load->model('cliente');
    //$this->load->model('pelicula');
    $this->load->model('genero');
    //verificar si existe o no alguien conectado
    if (!$this->session->userdata("usuarioC0nectado")) {
      // code...
      $this->session->set_flashdata("error","Por favor ingrese al sistema");
      redirect('seguridades/cerrarSesion');
    }else { //Codigo cuando si esta conectado
      if ($this->session->userdata("usuarioC0nectado")['perfil']!="ADMINISTRADOR") {
        redirect('seguridades/cerrarSesion');
      }
    }
  }
  public function index()
  {
    $data["listadoGeneros"]=$this->genero->obtenerTodos();
    //Cargando la vista index
    //carpeta/archivo
    $this->load->view('encabezado');
    $this->load->view('generos/index');//pasando parametros a la vista
    $this->load->view('pie');
  }

  public function tablaGenero()
  {
    $data["listadoGeneros"]=$this->genero->obtenerTodos();
    $this->load->view('generos/tablaGenero',$data);//pasando parametros a la vista
  }
  public function nuevo()
  {
    //Cargando la vista index
    //carpeta/archivo
    $this->load->view('encabezado');
    $this->load->view('generos/nuevo');
    $this->load->view('pie');
  }
  public function guardarGenero(){

    //capturar valores de la vista
      $genero=$this->input->post('genero_pel');
      /*echo $email;
      echo "<br>";
      echo $password;*/
      //armando arreglo para insertar datos en la BDD
      $datosNuevoGenero=array(
        "genero_pel"=>$genero
      );
      if ($this->genero->insertar($datosNuevoGenero)) {
        //si es verdadero si se inserto
        $this->session->set_flashdata("confirmacion","Genero registrado exitosamente");
        redirect('generos/index');
      }else {
        //si es falso no se inserto
        echo "Genero no guardado";
      }
  }
  public function eliminarGenero($id){
    //validando si la eliminacion se realiza o no
    if ($this->genero->eliminarPorId($id)) {
      $this->session->set_flashdata("confirmacion","Genero eliminado exitosamente");
      redirect('generos/index');
    }else {
      echo 'Error al eliminar';
    }
  }
  public function editar($id){
    $data['generoEditar']=$this->genero->obtenerPorId($id);
    //Cargando la vista index
    //carpeta/archivo
    $this->load->view('encabezado');
    $this->load->view('generos/editar',$data);
    $this->load->view('pie');
  }
  public function actualizarGenero(){
    $id_gen=$this->input->post('id_gen');//captura el id del cliente a EDITAR
    $datosEditados=array(
      "genero_pel"=>$this->input->post('genero_pel')
    );
    if($this->genero->actualizar($id_gen,$datosEditados)){
      $this->session->set_flashdata("confirmacion","Genero actualizado exitosamente");
      redirect('generos/index');
    }else{
      $this->session->set_flashdata("confirmacion","Genero actualizado exitosamente");
      redirect('generos/index');
    }
  }
}
