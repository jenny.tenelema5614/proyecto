<?php
  class Gestiones extends CI_Controller{

    //Constructor de la clase (Ojo doble guion bajo __)
    public function __construct(){
      parent::__construct();
      //cargar la libreria en este controlador
      $this->load->database();
      $this->load->library('grocery_CRUD');

      //Verificar si existe o no alguien conectado
      if(!$this->session->userdata("usuarioC0nectado")){
          $this->session->set_flashdata("error","Por favor Inicie Sesion");
          redirect('seguridades/cerrarSesion');
      }else{//Codigo cuando SI esta conectado
        if(!($this->session->userdata("usuarioC0nectado")["perfil"]=="ADMINISTRADOR"
          || $this->session->userdata("usuarioC0nectado")["perfil"]=="VENDEDOR")){
            redirect('seguridades/cerrarSesion');
        }
      }
    }

    public function gestionClientes(){
      $clientes=new grocery_CRUD();
      $clientes->set_table('cliente');  // nombre d ela base de datos
      $clientes->set_language('spanish');
      $clientes->set_theme('flexigrid');
      $clientes->display_as("codigo_cli","CÓDIGO");
      $clientes->display_as("cedula_cli","CÉDULA");
      $clientes->display_as("apellidos_cli","APELLIDOS"); // flexigrid o datatables
      $clientes->display_as("nombres_cli","NOMBRE"); // fl
      $clientes->display_as("direccion_cli","DIRECCIÓN"); // fl
      $clientes->display_as("telefono_convencional_cli","TELÉFONO");
      $clientes->display_as("telefono_celular_cli","CELULAR"); // fl
      $clientes->display_as("fecha_creacion_cli","FECHA CREACIÓN"); // fl
      $clientes->display_as("fecha_actualizacion_cli","FECHA   ACTUALIZACIÓN"); // fl
      $clientes->set_subject("Cliente");
      $clientes->columns('cedula_cli','apellidos_cli','nombres_cli');
      $clientes->fields('cedula_cli','apellidos_cli','nombres_cli','direccion_cli','telefono_convencional_cli','telefono_celular_cli');
      $clientes->required_fields('cedula_cli','apellidos_cli','nombres_cli','direccion_cli','telefono_celular_cli');

      $output=$clientes->render();
      $this->load->view('encabezado');
      $this->load->view('gestiones/gestionClientes',$output);
      $this->load->view('pie');
    }




  }
 ?>
