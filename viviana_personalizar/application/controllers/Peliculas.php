<?php
class Peliculas extends CI_Controller{
  //constructor de la clase
  public function __construct(){
    parent::__construct();
    //cargando modelo pelicula
    $this->load->model('pelicula');
    $this->load->model('genero');
    //verificar si existe o no alguien conectado
    if (!$this->session->userdata("usuarioC0nectado")) {
      // code...
      $this->session->set_flashdata("error","Por favor ingrese al sistema");
      redirect('seguridades/cerrarSesion');
    }else { //Codigo cuando si esta conectado
      if ($this->session->userdata("usuarioC0nectado")['perfil']!="ADMINISTRADOR") {
        redirect('seguridades/cerrarSesion');
      }
    }
  }
/*Funcion que renderiza el listado
de peluculas*/
  public function index()
  {
    //Cargando la vista index
    $data["listadoPeliculas"]=$this->pelicula->obtenerTodos();
    //carpeta/archivo
    $this->load->view('encabezado');
    $this->load->view('peliculas/index',$data);//pasando parametros a la vista
    $this->load->view('pie');
  }
  public function nuevo()
  {
    //Cargando la vista index
    //carpeta/archivo
    $data["listadoGeneros"]=$this->genero->obtenerTodos();
    $this->load->view('encabezado');
    $this->load->view('peliculas/nuevo',$data);
    $this->load->view('pie');
  }
  public function guardarPelicula(){

      /*INICIO PROCESO DE SUBIDA DE ARCHIVO*/
      $config['upload_path']=APPPATH.'../uploads/'; //ruta de subida de archivos
      $config['allowed_types']='jpeg|jpg|png'; //tipos de archivos permitidos
      $config['max_size']=5*1024; //definir peso maximo de subida (5MB)
      $nombre_aleatorio="pelicula_".time()*rand(100,10000); //creando un nombre aleatorio
      $config['file_name']=$nombre_aleatorio; //asignano el nombre al archivo subido
      $this->load->library('upload',$config); //cargando la libreria upload
      if ($this->upload->do_upload("imagen_portada_pel")) { //intentamos subir el archivo
        $dataArchivoSubido=$this->upload->data(); //capturando la información del archivo subido
        $nombre_archivo_subido=$dataArchivoSubido["file_name"]; //obteniendo el nombre del archivo
      }else {
        $nombre_archivo_subido=""; //cuando no se sube el archivo el nombre queda vacio
      }


      /*FIN PROCESO DE SUBIDA DE ARCHIVO*/

    //capturar valores de la vista
      $titulo=$this->input->post('titulo_pel');
      $duracion=$this->input->post('duracion_pel');
      $director=$this->input->post('director_pel');
      $genero=$this->input->post('fk_id_gen');
      $costo_alquiler=$this->input->post('costo_alquiler_pel');
      /*echo $email;
      echo "<br>";
      echo $password;*/
      //armando arreglo para insertar datos en la BDD
      $datosNuevoPelicula=array(
        "titulo_pel"=>$titulo,
        "duracion_pel"=>$duracion,
        "director_pel"=>$director,
        "fk_id_gen"=>$genero,
        "costo_alquiler_pel"=>$costo_alquiler,
        "imagen_portada_pel"=>$nombre_archivo_subido
      );
      if ($this->pelicula->insertar($datosNuevoPelicula)) {
        //si es verdadero si se inserto
        $this->session->set_flashdata("confirmacion","Pelicula registrada exitosamente");
        redirect('peliculas/index');
      }else {
        //si es falso no se inserto
        echo "Pelicula no guardado";
      }
  }
  //metodo para eliminar pelicula recibiendo como parametro su id
  public function eliminarPelicula($id){
    $data['peliculaEditar']=$this->pelicula->obtenerPorId($id);
    $id_img=$data['peliculaEditar']->imagen_portada_pel;
    //validando si la eliminacion se realiza o no
    unlink(APPPATH.'../uploads/'.$id_img);
    if ($this->pelicula->eliminarPorId($id)) {
      $this->session->set_flashdata("confirmacion","Pelicula eliminada exitosamente");
      redirect('peliculas/index');
    }else {
      echo 'Error al eliminar';
    }
  }
  public function editar($id){
    $data["listadoGeneros"]=$this->genero->obtenerTodos();
    $data['peliculaEditar']=$this->pelicula->obtenerPorId($id);
    //Cargando la vista index
    //carpeta/archivo
    $this->load->view('encabezado');
    $this->load->view('peliculas/editar',$data);
    $this->load->view('pie');
  }
  public function imagen($id){
    //$data["listadoGeneros"]=$this->genero->obtenerTodos();
    $data['peliculaEditar']=$this->pelicula->obtenerPorId($id);
    //Cargando la vista index
    //carpeta/archivo
    $this->load->view('encabezado');
    $this->load->view('peliculas/imagen',$data);
    $this->load->view('pie');
  }
  //metodo para llamar a la actualizacion del modelo
  public function actualizarPelicula(){
    $id_img=$this->input->post('id_img');
    /*INICIO PROCESO DE SUBIDA DE ARCHIVO*/
    $config['upload_path']=APPPATH.'../uploads/'; //ruta de subida de archivos
    $config['allowed_types']='jpeg|jpg|png'; //tipos de archivos permitidos
    $config['max_size']=5*1024; //definir peso maximo de subida (5MB)
    if ($id_img=="") {
      // code...
      $nombre_aleatorio="pelicula_".time()*rand(100,10000); //creando un nombre aleatorio
      $config['file_name']=$nombre_aleatorio; //asignano el nombre al archivo subido
    }else {
      unlink(  APPPATH.'../uploads/'.$id_img);// si es true, llama la función
      $config['file_name']=$id_img; //asignano el nombre al archivo subido
    }



    $this->load->library('upload',$config); //cargando la libreria upload
    if ($this->upload->do_upload("imagen_portada_pel")) { //intentamos subir el archivo
      $dataArchivoSubido=$this->upload->data(); //capturando la información del archivo subido
      $nombre_archivo_subido=$dataArchivoSubido["file_name"]; //obteniendo el nombre del archivo
    }else {
      $nombre_archivo_subido=""; //cuando no se sube el archivo el nombre queda vacio
    }


    /*FIN PROCESO DE SUBIDA DE ARCHIVO*/
    $id_pel=$this->input->post('id_pel');//captura el id del pelicula a EDITAR
    $datosEditados=array(
      "titulo_pel"=>$this->input->post('titulo_pel'),
      "duracion_pel"=>$this->input->post('duracion_pel'),
      "director_pel"=>$this->input->post('director_pel'),
      "fk_id_gen"=>$this->input->post('fk_id_gen'),
      "costo_alquiler_pel"=>$this->input->post('costo_alquiler_pel'),
      "imagen_portada_pel"=>$nombre_archivo_subido,
      "fecha_actualizacion_pel"=>date('Y-m-d H:i:s')
    );
    if($this->pelicula->actualizar($id_pel,$datosEditados)){
      $this->session->set_flashdata("confirmacion","Pelicula actualizada exitosamente");
      redirect('peliculas/index/');
    }else{
      echo "Error al actualizar";
    }
  }
  public function borrarPortada(){
    $id_img=$this->input->post('id_img');
    unlink(  APPPATH.'../uploads/'.$id_img);// si es true, llama la función


    /*FIN PROCESO DE SUBIDA DE ARCHIVO*/
    $id_pel=$this->input->post('id_pel');//captura el id del pelicula a EDITAR
    $datosEditados=array(
      "imagen_portada_pel"=>""
    );
    if($this->pelicula->actualizar($id_pel,$datosEditados)){
      $this->session->set_flashdata("confirmacion","Pelicula actualizada exitosamente");
      redirect('peliculas/index/');
    }else{
      echo "Error al actualizar";
    }
  }


  public function validarTituloExistente(){
          $titulo_pel=$this->input->post('titulo_pel');
         $peliculaExistente=$this->pelicula->consultarTituloPor($titulo_pel);
         if($peliculaExistente){
           echo json_encode(FALSE);
         }else{
           echo json_encode(TRUE);
         }
     }
  }
?>
