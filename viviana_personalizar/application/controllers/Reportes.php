<?php
  class Reportes extends CI_Controller{
    public function __construct(){
      parent::__construct();
      $this->load->model('pelicula');
        $this->load->model('cliente');
      if(!$this->session->userdata("usuarioC0nectado")){
          $this->session->set_flashdata("error","Por favor Inicie Sesion");
          redirect('seguridades/cerrarSesion');
      }else{
        if($this->session->userdata("usuarioC0nectado")["perfil"]!="ADMINISTRADOR"){
            redirect('seguridades/cerrarSesion');
        }
      }
    }
    public function index(){

      $data["clientes"]=$this->cliente->obtenerTodos();
      $data["peliculas"]=$this->pelicula->obtenerTodos();
      $this->load->view('encabezado');
        $this->load->view('reportes/index',$data);
          $this->load->view('pie');


    }
  }
