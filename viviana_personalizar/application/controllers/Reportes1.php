<?php
  class Reportes1 extends CI_Controller{
    public function __construct(){
      parent::__construct();
      $this->load->model('genero');
        $this->load->model('alquiler');
      if(!$this->session->userdata("usuarioC0nectado")){
          $this->session->set_flashdata("error","Por favor Inicie Sesion");
          redirect('seguridades/cerrarSesion');
      }else{
        if($this->session->userdata("usuarioC0nectado")["perfil"]!="ADMINISTRADOR"){
            redirect('seguridades/cerrarSesion');
        }
      }
    }

    public function nuevo(){
      $data["generos"]=$this->genero->obtenerTodos();
      $data["alquileres"]=$this->alquiler->obtenerTodos();
      $this->load->view('encabezado');
        $this->load->view('reportes1/nuevo',$data);
          $this->load->view('pie');
    }
  }
