<?php
class Seguridades extends CI_Controller{
  //constructor de la clase
  public function __construct(){
    parent::__construct();
    $this->load->model('usuario');
    //cargando modelo usuario
  }
  //Funcion se encarga de renderizar el formulario de inicio de sesion o loggin
  public function login(){
    $this->load->view('encabezado');
    $this->load->view('seguridades/login');
    $this->load->view('pie');
  }
  public function autenticarUsuario()
  {
    // code..
    $email=$this->input->post('email_usu');
    $password=$this->input->post('password_usu');
    $usuarioConsultado=$this->usuario->consultarPorEmailPassword($email,$password);
    if ($usuarioConsultado) {
      // code...
      //echo "Email y contraseña Correctos";
      //Creando una variable de sesion -> userdata
      $datosSession=array(
        "id"=>$usuarioConsultado->id_usu,
        "email"=>$usuarioConsultado->email_usu,
        "perfil"=>$usuarioConsultado->perfil_usu
      ); //Array que contiene los valores de la sesion
      $this->session->set_userdata("usuarioC0nectado",$datosSession); //Creando sesion
      $this->session->set_flashdata("confirmacion","Acceso exitoso, bienvenido al sistema");
      redirect("/");
    }else {
      // code...
      $this->session->set_flashdata("error","Email o contraseña incorrectos");
      redirect('seguridades/login');
    }
  }
  public function cerrarSesion(){
    $this->session->sess_destroy(); //borrar todos los datos de session
    redirect('seguridades/login');
  }
}
?>
