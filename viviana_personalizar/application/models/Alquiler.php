<?php
  class Alquiler extends CI_Model{
    public function insertar($data){
      return $this->db->insert('alquiler',$data);
    }
    public function obtenerTodos(){
      $this->db->join('cliente','cliente.id_cli=alquiler.fk_id_cli');
      $this->db->join('pelicula','pelicula.id_pel=alquiler.fk_id_pel');
      $query=$this->db->get('alquiler');
      if ($query->num_rows()>0) {
        return $query;//cuando si hay registros en la BDD
      }else {
        return false;//cuando no hay registros
      }
    }
    public function obtenerPorId($id){
      $this->db->where('id_alqui',$id);
      $query=$this->db->get('alquiler');
      if ($query->num_rows()>0) {
        return $query->row();//cuando si hay registros en la BDD
      }else {
        return false;//cuando no hay registros
      }
    }
    public function eliminarPorId($id_alqui)
    {
      $this->db->where('id_alqui',$id_alqui);
      return $this->db->delete('alquiler');
    }
    public function actualizar($id,$datosAlquiler){
      $this->db->where('id_alqui',$id);
      return $this->db->update('alquiler',$datosAlquiler);
    }
  }
