<?php
  class Cliente extends CI_Model{
    //funcion para inserta nuevo clientes
    public function insertar($datosCliente){
      return $this->db->insert('cliente',$datosCliente);
    }
    public function obtenerTodosPorIdUsuario($id_usu){
    $this->db->where("fk_id_usu",$id_usu);//filtrando de acuerdo al usuario conectado
    $query=$this->db->get('cliente');
    if ($query->num_rows()>0){
      return $query; //cuando SI hay registros en la bdd
    }else {
      return false; //cuando NO hay registros en la bdd
    }
  }
    //funcion para consultar datos de BDD
    public function obtenerTodos(){
      $query=$this->db->get('cliente');
      if ($query->num_rows()>0) {
        return $query;//cuando si hay registros en la BDD
      }else {
        return false;//cuando no hay registros
      }
    }
    //funcion para consultar datos de BDD por id
    public function obtenerPorId($id){
      $this->db->where('id_cli',$id);
      $query=$this->db->get('cliente');
      if ($query->num_rows()>0) {
        return $query->row();//cuando si hay registros en la BDD
      }else {
        return false;//cuando no hay registros
      }
    }
    //metodo para eliminar cliente recibiendo
    //como parametro su idea
    //funcion para eliminar clientes
  public function eliminarPorId($id_cli)
  {
    $this->db->where('id_cli',$id_cli);
    return $this->db->delete('cliente');
  }
  //funcion para procesar la actualizacion del clientes
  public function actualizar($id,$datosCliente){
    $this->db->where('id_cli',$id);
    return $this->db->update('cliente',$datosCliente);
  }
  public function consultarClientePorCedula($cedula_cli) {
      $this->db->where('cedula_cli',$cedula_cli);
      $query=$this->db->get('cliente');
      if ($query->num_rows()>0) {
        return $query->row();//cuando si hay registros en la BDD
      }else {
        return false;//cuando no hay registros
      }

  }
  }
 ?>
