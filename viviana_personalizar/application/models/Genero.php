<?php
  class Genero extends CI_Model{
    public function obtenerTodos(){
      $query=$this->db->get('genero');
      if ($query->num_rows()>0) {
        return $query;//cuando si hay registros en la BDD
      }else {
        return false;//cuando no hay registros
      }
    }
    public function obtenerPorId($id){
      $this->db->where('id_gen',$id);
      $query=$this->db->get('genero');
      if ($query->num_rows()>0) {
        return $query->row();//cuando si hay registros en la BDD
      }else {
        return false;//cuando no hay registros
      }
    }
    public function insertar($datosGenero){
      return $this->db->insert('genero',$datosGenero);
    }
    public function eliminarPorId($id_gen)
    {
      $this->db->where('id_gen',$id_gen);
      return $this->db->delete('genero');
    }
    public function actualizar($id,$datosGenero){
      $this->db->where('id_gen',$id);
      return $this->db->update('genero',$datosGenero);
    }
  }
