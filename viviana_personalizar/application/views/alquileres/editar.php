
  <section class="blog-page">
    <div class="container">
  <div class="row">
    <div class="col-md-12">
  <form class="newsletter-form" action="<?php echo site_url(); ?>/alquileres/actualizarAlquiler" method="post" id="frm_editar_alquiler">
    <input type="hidden" name="id_alqui" id="id_alqui" class="form-control"
    value="<?php echo $alquilerEditar->id_alqui; ?>" placeholder="Ingrese su id">
    <table class="">
      <tr>

				<h3><font color="blue" >EDITAR ALQUILER</font></h3><br>
				<br>
				<br>
        <td><label for=""><h5><font color="black">CLIENTE:</font></h5></label></td>
        <td><select class="form-control" name="fk_id_cli" id="fk_id_cli" required>
              <option value="">--Seleccione--</option>
              <?php if ($listadoClientes): ?>
                <?php foreach ($listadoClientes->result() as $clienteTemporal): ?>
                  <option value="<?php echo $clienteTemporal->id_cli; ?>">
                    <?php echo $clienteTemporal->cedula_cli; ?> - <?php echo $clienteTemporal->apellidos_cli; ?> <?php echo $clienteTemporal->nombres_cli; ?>
                  </option>
                <?php endforeach; ?>
              <?php endif; ?>
          </select></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td><label for=""><h5><font color="black">PELICULA:</font></h5></td>
          <td><select class="form-control" name="fk_id_pel" id="fk_id_pel" required>
                <option value="">--Seleccione--</option>
                <?php if ($listadoPeliculas): ?>
                  <?php foreach ($listadoPeliculas->result() as $peliculaTemporal): ?>
                    <option value="<?php echo $peliculaTemporal->id_pel; ?>">
                      <?php echo $peliculaTemporal->titulo_pel; ?> - <?php echo $peliculaTemporal->costo_alquiler_pel; ?> USD
                    </option>
                  <?php endforeach; ?>
                <?php endif; ?>
            </select><td>
      </tr>
      <tr>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td><label for=""><h5><font color="black">FECHA INICIO:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</font></h5></label></td>
        <td><input type="date" name="fecha_inicio_alqui" id="fecha_inicio_alqui" value="<?php echo $alquilerEditar->fecha_inicio_alqui; ?>" required class="form-control"></td>
      </tr>
      <tr>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td><label for=""><h5><font color="black">FECHA FIN:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</font></h5></label></td>
        <td><input type="date" name="fecha_fin_alqui" id="fecha_fin_alqui" value="<?php echo $alquilerEditar->fecha_fin_alqui; ?>" required class="form-control"></td>
      </tr>
      <tr>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td><label for=""><h5><font color="black">PRECIO:</font></h5></label></td>
        <td><input type="number" placeholder="Ingrese el precio" name="costo_alquiler_pel" id="costo_alquiler_pel" placeholder="Ingrese un precio" value="<?php echo $alquilerEditar->precio_alqui; ?>" class="form-control" required></td>
      </tr>
      <tr>
        <td>&nbsp;</td>
      </tr>
    </table><center>
      <button type="submit" name="button" class="site-btn">GUARDAR ALQUILER</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <a href="<?php echo site_url(); ?>" class="site-btn">CANCELAR</a><center>
<div>
  </div>
  </form>
</div>
</section>
  <script type="text/javascript">
    $("#fk_id_pel").val('<?php echo $alquilerEditar->fk_id_pel; ?>');
  </script>
  <script type="text/javascript">
    $("#fk_id_cli").val('<?php echo $alquilerEditar->fk_id_cli; ?>');
  </script>
<script type="text/javascript">
  $("#frm_editar_alquiler").validate({
    rules:{
      costo_alquiler_pel:{
        required:true,
        digits:true,
        maxlength:3,
        minlength:1
      },
      fk_id_pel:{
        required:true
      }
    },
    messages:{
      costo_alquiler_pel:{
        required:"Por favor ingrese el precio",
        digits:"Por favor ingrese solo numeros",
        maxlength:"Por favor ingrese 3 digitos",
        minlength:"Por favor ingrese 3 digitos"
      },
      fk_id_pel:{
        required:"Por favor ingrese el genero"
      }
    }
  });
</script>
