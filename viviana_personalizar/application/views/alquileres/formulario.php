
<div class="row">
  <div class="col-md-12">
    <form class="newsletter-form" action="<?php echo site_url(); ?>/alquileres/insertarAlquiler" method="post" id="frm_nuevo_alquiler">
      <table class="">
        <tr>
					<center>
					  <legend><h3><font color="blue"> FORMULARIO DE ALQUILERES</font></h3><br></legend>
					</center>
          <td><label for=""><h5><font color="black">CLIENTE:</font></h5></label></td>
          <td><select class="form-control" name="fk_id_cli" id="fk_id_cli" required>
              <option value="">--Seleccione--</option>
              <?php if ($listadoClientes): ?>
                <?php foreach ($listadoClientes->result() as $clienteTemporal): ?>
                  <option value="<?php echo $clienteTemporal->id_cli; ?>">
                    <?php echo $clienteTemporal->cedula_cli; ?> - <?php echo $clienteTemporal->apellidos_cli; ?> <?php echo $clienteTemporal->nombres_cli; ?>
                  </option>
                <?php endforeach; ?>
              <?php endif; ?>
          </select></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td><label for=""><h5><font color="black">PELICULA:</font></h5></td>
          <td><select class="form-control" name="fk_id_pel" id="fk_id_pel" required>
              <option value="">--Seleccione--</option>
              <?php if ($listadoPeliculas): ?>
                <?php foreach ($listadoPeliculas->result() as $peliculaTemporal): ?>
                  <option value="<?php echo $peliculaTemporal->id_pel; ?>">
                    <?php echo $peliculaTemporal->titulo_pel; ?> - <?php echo $peliculaTemporal->costo_alquiler_pel; ?> USD
                  </option>
                <?php endforeach; ?>
              <?php endif; ?>
          </select></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td><label for=""><h5><font color="black">FECHA INICIO:</font></h5></label></td>
          <td><input type="date" name="fecha_inicio_alqui" id="fecha_inicio_alqui" value="" required class="form-control" required></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td><label for=""><h5><font color="black">FECHA FIN:</font></h5></label></td>
          <td><input type="date" name="fecha_fin_alqui" id="fecha_fin_alqui" value="" required class="form-control"></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
    </table><center>
    <button type="submit" name="button" class="site-btn">GUARDAR ALQUILER</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
      <a href="<?php echo site_url(); ?>" class="site-btn">CANCELAR</a>
    </center></div>
</div>
  </div>
</form>
</section>
<style media="screen">
  span.error{ color: red; }
</style>
<script type="text/javascript">
  $("#frm_nuevo_alquiler").validate({
    rules:{
			fk_id_cli:{
        required:true,
      },
			fk_id_pel:{
        required:true,
      },
      fecha_inicio_alqui:{
        required:true,
      },
			fecha_fin_alqui:{
        required:true,
      }
      
    },
    messages:{
			fk_id_cli:{
				required:"<br>Por favor seleccione el cliente",
			},
			fk_id_pel:{
				required:"<br>Por favor seleccione la pelicula",
			},
      fecha_inicio_alqui:{
        required:"<br>Por favor ingrese la fecha de inicio de alquiler",
      },
			fecha_fin_alqui:{
        required:"<br>Por favor ingrese la fecha de fin de alquiler",
      }
    },
      errorElement : 'span'
  });
</script>
