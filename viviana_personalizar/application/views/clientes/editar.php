
  <section class="contact-page">
    <div class="container">
<div class="row">
  <div class="col-md-12 text-center">
    <legend><h2>
    <font color="blue">
         FORMULARIO DE EDITAR CLIENTE</font></h2>
    </legend>
  </div>
<div class="row">
  <div class="col-md-12">
    <form class="newsletter-form" action="<?php echo site_url(); ?>/clientes/actualizarCliente" method="post" id="frm_editar_cliente">
      <input type="hidden" name="id_cli" id="id_cli" class="form-control"
      value="<?php echo $clienteEditar->id_cli; ?>" placeholder="Ingrese su id"><br>
      <table class="">
        <tr>
          <td><label for=""><h4><font color="black">Cédula:</font></h4></label></td>
          <td><input type="number" name="cedula_cli" id="cedula_cli" class="form-control"
          value="<?php echo $clienteEditar->cedula_cli; ?>" placeholder="Ingrese su cedula" required></td>
        </tr>
        <tr>
          <td></td>
          <td><br><font color="gray">Ej. 1718192023</font></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td><label for=""><h4><font color="black">Nombres:</font><h4></label></td>
          <td><input type="nombres" name="nombres_cli" id="nombres_cli" class="form-control"
          value="<?php echo $clienteEditar->nombres_cli; ?>" placeholder="Ingrese los  nombres" required></td>
        </tr>
        <tr>
          <td></td>
          <td><br><font color="gray">Ej. Juan Carlos</font></td>
        </tr>
      </tr>
      <tr>
        <td>&nbsp;</td>
      </tr>
        <tr>
          <td><label for=""><h4><font color="black">Apellidos:</font></h4></label></td>
          <td><input type="apellidos" name="apellidos_cli" id="apellidos_cli" class="form-control"
          value="<?php echo $clienteEditar->apellidos_cli; ?>" placeholder="Ingrese los apellidos" required></td>
        </tr>
        <tr>
          <td></td>
          <td><br><font color="gray">Ej. Marinez Arias</font></td>
        </tr>
      </tr>
      <tr>
        <td>&nbsp;</td>
      </tr>
        <tr>
        <td><label for=""><h4><font color="black">Dirección:</font></h4></label></td>
          <td><input type="direccion" name="direccion_cli" id="direccion_cli" class="form-control"
          value="<?php echo $clienteEditar->direccion_cli; ?>" placeholder="Ingrese sus dirección" required></td>
        </tr>
        <tr>
          <td></td>
          <td><br><font color="gray">Ej. Latacunga</font></td>
        </tr>
      </tr>
      <tr>
        <td>&nbsp;</td>
      </tr>
        <tr>
          <td><label for=""><h4><font color="black">Teléfono Convencional:</font></h4></label></td>
          <td><input type="number" name="telefono_convencional_cli" id="telefono_convencional_cli" class="form-control"
          value="<?php echo $clienteEditar->telefono_convencional_cli; ?>" placeholder="Ingrese el  teléfono convencional"></td>
        </tr>
        <tr>
          <td></td>
          <td><br><font color="gray">Ej. 032667788</font></td>
        </tr>
      </tr>
      <tr>
        <td>&nbsp;</td>
      </tr>
        <tr>
          <td><label for=""><h4><font color="black">Teléfono Celular:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</font></h4></label></td>
          <td><input type="number" name="telefono_celular_cli" id="telefono_celular_cli" class="form-control"
          value="<?php echo $clienteEditar->telefono_celular_cli; ?>" placeholder="Ingrese el numero de celular" required></td>
        </tr>
        <tr>
          <td></td>
          <td><br><font color="gray">Ej. 0987654321</font></td>
        </tr>
      </tr>
      <tr>
        <td>&nbsp;</td>
      </tr>
      </table><center>
      <button type="submit" name="button" class="site-btn">
        <i class="glyphicon glyphicon-ok"></i>
        Guardar</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
      <a href="<?php echo site_url(); ?>/clientes/index" class="site-btn">
        <i class="glyphicon glyphicon-remove"></i>
        Cancelar</a></center>
    </form>
  </div>
  <div class="col-md-3">

  </div>
</div>
</div>
</section>
<style media="screen">
  span.error{ color: red; }
</style>
<script type="text/javascript">
  $("#frm_editar_cliente").validate({
    rules:{
      cedula_cli:{
        required:true,
        digits:true,
        maxlength:10,
        minlength:10
      },
      apellidos_cli:{
        required:true
      },
      nombres_cli:{
        required:true
      },
      direccion_cli:{
        required:true
      },
      telefono_celular_cli:{
        required:true
      }
    },
    messages:{
      cedula_cli:{
        required:"<br>Por favor ingrese la cedula",
        digits:"<br>Por favor ingrese solo numeros",
        maxlength:"<br>Por favor ingrese 10 digitos",
        minlength:"<br>Por favor ingrese 10 digitos"
      },
      apellidos_cli:{
        required:"<br>Por favor ingrese los apellidos"
      },
      nombres_cli:{
        required:"<br>Por favor ingrese los nombres"
      },
      direccion_cli:{
        required:"<br>Por favor ingrese la dirección"
      },
      telefono_celular_cli:{
        required:"<br>Por favor ingrese su teléfono celular"
      }
    },
		errorElement : 'span'
  });
</script>
