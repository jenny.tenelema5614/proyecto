<center><h3><FONT COLOR="blue">CLIENTE REGISTRADO</FONT></h3><br></center>
<?php if ($listadoClientes): ?>
    <table class="table table-bordered table-striped table-danger">
    <thead>
      <tr>
        <th class="text-center">ID</th>
        <th class="text-center">CEDULA</th>
        <th class="text-center">APELLIDOS</th>
        <th class="text-center">NOMBRES</th>
        <th class="text-center">DIRECCIÓN</th>
        <th class="text-center">TELÉFONO CONVENCIONAL</th>
        <th class="text-center">TELÉFONO CELULAR</th>
        <th class="text-center">ACCIONES</th>
      </tr>
  </thead>
    <tbody>
      <?php foreach ($listadoClientes->result() as $clienteTemporal): ?>
        <tr>
          <td class="text-center"><?php echo $clienteTemporal->id_cli ?></td>
          <td class="text-center"><?php echo $clienteTemporal->cedula_cli ?></td>
          <td class="text-center"><?php echo $clienteTemporal->apellidos_cli ?></td>
          <td class="text-center"><?php echo $clienteTemporal->nombres_cli ?></td>
          <td class="text-center"><?php echo $clienteTemporal->direccion_cli ?></td>
          <td class="text-center"><?php echo $clienteTemporal->telefono_convencional_cli ?></td>
          <td class="text-center"><?php echo $clienteTemporal->telefono_celular_cli ?></td>
          <td class="text-center">

              <a href="<?php echo site_url(); ?>/clientes/editar/<?php echo $clienteTemporal->id_cli; ?>"class="fa fa-edit" title="Editar">
                  </a>
                  <a href="<?php echo site_url(); ?>/clientes/eliminarCliente/<?php echo $clienteTemporal->id_cli; ?>"
                      onclick="confirmation(event)">
                      <i class="fa fa-trash" title="ELIMINAR"></i>
                    </a>
            </td>
        </tr>
      <?php endforeach; ?>
    </tbody>
  </table>
<?php else: ?>
  <div class="alert alert-danger">
    No se encontraron clientes registrados
  </div>
<?php endif; ?>
