<!DOCTYPE html>
<html lang="es" dir="ltr">
<head>
	<title>SISTEMA DE PELÍCULAS </title>
	<meta charset="UTF-8">
	<meta name="description" content="SolMusic HTML Template">
	<meta name="keywords" content="music, html">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
  <script src="https://code.jquery.com/jquery-3.6.0.js
    "integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk="crossorigin="anonymous"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.3/dist/jquery.validate.js">
    </script>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>


	<!-- Favicon -->
	<link href="<?php echo base_url(); ?>/assets/img/favicon.ico" rel="shortcut icon"/>

	<!-- Google font -->
	<link href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,500,500i,600,600i,700,700i&display=swap" rel="stylesheet">

	<!-- Stylesheets -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>/assets/css/bootstrap.min.css"/>
	<link rel="stylesheet" href="<?php echo base_url(); ?>/assets/css/font-awesome.min.css"/>
	<link rel="stylesheet" href="<?php echo base_url(); ?>/assets/css/owl.carousel.min.css"/>
	<link rel="stylesheet" href="<?php echo base_url(); ?>/assets/css/slicknav.min.css"/>

	<!-- Main Stylesheets -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>/assets/css/style.css"/>




	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
<link rel="stylesheet" href="//cdn.datatables.net/1.11.4/css/jquery.dataTables.min.css">
<script type="text/javascript" src="//cdn.datatables.net/1.11.4/js/jquery.dataTables.min.js">
</script>


  <?php if ($this->session->flashdata('confirmacion')):?>
  	<script type="text/javascript">
  	$(document).ready(function(){
  		Swal.fire(
  		  'CONFIRMACION!',
  			'<?php echo
  			$this->session->flashdata('confirmacion');?>',

  			'success'
  		//  'warning'//icono de advertencia

  		);
  	});
  	</script>
  <?php endif; ?>


	<?php if ($this->session->flashdata('error')):?>
		<script type="text/javascript">
		$(document).ready(function(){
			Swal.fire(
				'ERROR!',
				'<?php echo
				$this->session->flashdata('error');?>',

				'error'

			);
		});
		</script>
	<?php endif; ?>

  <?php if ($this->session->flashdata('eliminacion')):?>
  <script type="text/javascript">
  Swal.fire({
  	title: 'Are you sure?',
  	text: "You won't be able to revert this!",
  	icon: 'warning',
  	showCancelButton: true,
  	confirmButtonColor: '#3085d6',
  	cancelButtonColor: '#d33',
  	confirmButtonText: 'Yes, delete it!'
  }).then((result) => {
  	if (result.isConfirmed) {
  		Swal.fire(
  			'Deleted!',
  			'Your file has been deleted.',
  			'success'
  		)
  	}
  })

  </script>
  <?php endif; ?>
	<script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
<body>
	<!-- Page Preloder -->
	<div id="preloder">
		<div class="loader"></div>
	</div>

	<!-- Header section -->
	<header class="header-section clearfix">
		<a href="<?php echo base_url(); ?>" class="site-logo">
			<img src="<?php echo base_url(); ?>/assets/img/logo2.png" alt="">
			<?php if ($this->session->userdata("usuarioC0nectado")): ?>
								 <?php echo $this->session->userdata("usuarioC0nectado")['perfil']; ?>
							<?php endif; ?>
		</a>

		</div>
		<ul class="main-menu">
			<li><a href="<?php echo base_url(); ?>">Inicio</a></li>

				<?php if ($this->session->userdata("usuarioC0nectado")): ?>
					<li ><a href=" <?php echo site_url()	?>/clientes/index">Gestión de clientes</a></li>
					<?php if ($this->session->userdata("usuarioC0nectado")["perfil"]=="ADMINISTRADOR"): ?>

	        	<li>
             <a href="<?php echo site_url()	?>/peliculas/index">
               Gestión Películas
             </a>
           </li>
					 <li>
						<a href="<?php echo site_url()	?>/reportes1/nuevo">
							Reportes
						</a>
					</li>
         <?php endif ?>

					 </li>

					 <li><a href="<?php echo site_url(); ?>/alquileres/index">Gestión alquiler</a></li>

					 <li><a href="<?php echo site_url(); ?>/alquileres/index">Gestión alquilerjv</a></li>

						 <li><a href="<?php echo site_url(); ?>/generos/index">Gestión género</a></li>
						 <li>
							 <a href="<?php echo site_url(); ?>/seguridades/cerrarSesion">
								 <?php echo $this->session->userdata("usuarioC0nectado")['email']; ?>
								 &nbsp;
								 &nbsp;
								 Salir
							 </a>
						 </li>
					<?php else: ?>
							<li><a href="<?php echo site_url(); ?>/seguridades/login">Ingresar</a></li>
					<?php endif; ?>

		</ul>
	</header>
	<!-- Header section end -->
