<center><h3><font color="blue"> GÉNERO REGISTRADO</font></h3></center><br>

<?php if ($listadoGeneros): ?>
    <table class="table table-bordered table-striped table-danger">
    <thead>
      <tr>
        <th class="text-center">ID</th>
        <th class="text-center">GENERO</th>
        <th class="text-center">ACCIONES</th>
      </tr>
  </thead>
    <tbody>
      <?php foreach ($listadoGeneros->result() as $generoTemporal): ?>
        <tr>
          <td class="text-center"><?php echo $generoTemporal->id_gen ?></td>
          <td class="text-center"><?php echo $generoTemporal->genero_pel ?></td>
          <td class="text-center">
            <a href="<?php echo site_url(); ?>/generos/editar/<?php echo $generoTemporal->id_gen ?>"><i class="fa fa-pencil" title="Editar"></i>Editar</a>
            <a href="<?php echo site_url(); ?>/generos/eliminarGenero/<?php echo $generoTemporal->id_gen ?>" onclick="confirmation(event)"><i class="fa fa-trash" title="Eliminar"></i>

          </a>  </td>
        </tr>
      <?php endforeach; ?>
    </tbody>
  </table>
<?php else: ?>
  <div class="alert alert-danger">
    No se encontraron generos registrados
  </div>
<?php endif; ?>
