<!-- Page top section -->
	<section class="page-top-section set-bg" data-setbg="<?php echo base_url(); ?>assets/img/page-top-bg/1.jpg">
		<div class="page-info">
			<h2>Peliculas</h2>
			<div class="site-breadcrumb">
				<a href="<?php echo base_url(); ?>">Inicio</a>  /
				<span>Peliculas</span>
			</div>
		</div>
	</section>
	<!-- Page top end-->
  <section class="blog-page">
    <div class="container">
<div class="row">
  <div class="col-md-12 text-center">
    <legend><h2>
        <img src="<?php echo base_url(); ?>assets/img/icons/plus.png" title="Editar" width="30px">
        <font color="black">EDITAR PELICULA</font></h2><br>
    </legend>
  </div>
</div>
	<div class="row">
	  <div class="col-md-12">
			<center>
			<?php if ($peliculaEditar->imagen_portada_pel!=""): ?>
						<a target="_blank" href="<?php echo base_url('uploads').'/'.$peliculaEditar->imagen_portada_pel; ?>">
								<img src="<?php echo base_url('uploads').'/'.$peliculaEditar->imagen_portada_pel; ?>"
								title="<?php echo $peliculaEditar->titulo_pel ?>" width=""><br>
								</a>
					<?php else: ?>
									N/A
					<?php endif; ?></center>
			</div>
		</div>
<div class="row">
  <div class="col-md-12">
		<form class="newsletter-form" enctype="multipart/form-data" action="<?php echo site_url(); ?>/peliculas/borrarPortada" method="post" id="frm_editar_pelicula">
      <input type="hidden" name="id_pel" id="id_pel" class="form-control"
      value="<?php echo $peliculaEditar->id_pel; ?>" placeholder="Ingrese su id">
			<input type="hidden" name="id_img" id="id_img" class="form-control"
      value="<?php echo $peliculaEditar->imagen_portada_pel; ?>" placeholder="Ingrese su id"><br><br>
			<center><button type="submit" name="button" class="site-btn">
						<i class="glyphicon glyphicon-remove"></i>
						Borrar Portada</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		      <a href="<?php echo site_url(); ?>/peliculas/index" class="site-btn">
		        <i class="glyphicon glyphicon-remove"></i>
		        Cancelar</a></center></td>
		</form>
  </div>
</div>
</div>
</div>
</section>
<script type="text/javascript">
	$("#fk_id_gen").val('<?php echo $peliculaEditar->fk_id_gen; ?>');
</script>
<style media="screen">
  span.error{ color: red; }
</style>
<script type="text/javascript">
  $("#frm_editar_pelicula").validate({
		rules:{
	    titulo_pel:{
	      required:true
	    },
	    duracion_pel:{
	      required:true,
	      digits:true,
	      maxlength:4
	    },
	    director_pel:{
	      required:true
	    },
			fk_id_gen:{
	      required:true
			},
	    costo_alquiler_pel:{
	      required:true,
	      digits:true,
	      maxlength:3
	    }
	  },
	  messages:{
	    titulo_pel:{
	      required:"<br>Por favor ingrese el título"
	    },
	    duracion_pel:{
	      required:"<br>Por favor ingrese la duración",
	      digits:"<br>Por favor ingrese solo numeros",
	      maxlength:"<br>Por favor ingrese 4 digitos"
	    },
	    director_pel:{
	      required:"<br>Por favor ingrese el director"
	    },
			fk_id_gen:{
	      required:"<br>Por favor seleccione el genero"
			},
	    costo_alquiler_pel:{
	      required:"<br>Por favor ingrese el costo del alquiler",
	      digits:"<br>Por favor ingrese solo numeros",
	      maxlength:"<br>Por favor ingrese 3 digitos"
	    }
	  },
		errorElement : 'span'
	});
	</script>
