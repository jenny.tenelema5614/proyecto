
  <section class="blog-page">
    <div class="container">
<div class="row">
  <div class="col-md-4 text-center">
  </div>
<div class="row">
  <div class="col-md-12">
    <form class="newsletter-form" enctype="multipart/form-data" action="<?php echo site_url(); ?>/peliculas/guardarPelicula"
			method="post" id="frm_nueva_pelicula">
        <table class="">
          <tr>
						  <h2><font color="blue">NUEVA PELICULA</font></h2><br>
							<br>
							<br>
            <td><label for=""><h5><font color="black">Título:</font></h5></label></td>
            <td><input type="titulo" name="titulo_pel" id="titulo_pel" class="form-control"
            value="" placeholder="Ingrese el titulo" required></td>
          </tr>
          <tr>
            <td></td>
            <td><br><font color="gray">Ej. Frida </font></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td><label for=""><h5><font color="black">Duración:</font></h5></label></td>
            <td><input type="number" name="duracion_pel" id="duracion_pel" class="form-control"
            value="" placeholder="Ingrese la  duración de la pelicula" required></td>
          </tr>
          <tr>
            <td></td>
            <td><br><font color="gray">Ej. 120</font></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td><label for=""><h5><font color="black">Director:</font></h5></label></td>
            <td><input type="director" name="director_pel" id="director_pel" class="form-control"
            value="" placeholder="Ingrese el director" required></td>
          </tr>
          <tr>
            <td></td>
            <td><br><font color="gray">Ej. James Cameron</font></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td><label for=""><h5><font color="black">Género:</font></h5></label></td>
            <td><select class="form-control" name="fk_id_gen" id="fk_id_gen">
              <option value="">--Seleccione--</option>
              <?php if ($listadoGeneros): ?>
                <?php foreach ($listadoGeneros->result() as $generoTemporal): ?>
                  <option value="<?php echo $generoTemporal->id_gen; ?>">
                    <?php echo $generoTemporal->genero_pel; ?>
                  </option>
                <?php endforeach; ?>
              <?php endif; ?>
                </select></td>
          </tr>
      <tr>
        <td>&nbsp;</td>
      </tr>
      <tr>
          <tr>
            <td><label for=""><h5><font color="black">Costo Alquiler:</font></h5></label></td>
            <td><input type="number" name="costo_alquiler_pel" id="costo_alquiler_pel" class="form-control"
            value="" placeholder="Ingrese el costo de alquiler" required></td>
          </tr>
          <tr>
            <td></td>
            <td><br><font color="gray">Ej. 17</font></td>
          </tr>
						<tr>
			        <td>&nbsp;</td>
			      </tr>
			      <tr>
			          <tr>
			            <td><label for=""><h5><font color="black">Portada:</font></h5></label></td>
			            <td><input type="file" name="imagen_portada_pel" id="imagen_portada_pel" class="form-control"
			            value=""></td>
			          </tr>
			          <tr>
			            <td>&nbsp;</td>
			          </tr>
			          <tr>
          </table><center>
        <button type="submit" name="button" class="site-btn">
          <i class="glyphicon glyphicon-ok"></i>
          Guardar</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <a href="<?php echo site_url(); ?>/peliculas/index" class="site-btn">
          <i class="glyphicon glyphicon-remove"></i>
          Cancelar</a></center>
      </form>
  </div>
  <div class="col-md-3">
  </div>
</div>
</div>
</div>
</section>
<style media="screen">
  span.error{ color: red; }
</style>
<script type="text/javascript">
  $("#frm_nueva_pelicula").validate({
  rules:{
    titulo_pel:{
      required:true,
      remote:{
                  url:"<?php echo site_url('peliculas/validarTituloExistente'); ?>",
                  data:{
                    "titulo_pel":function(){
                      return $("#titulo_pel").val();
                    }
                  },
                  type:"post"
              }

    },
    duracion_pel:{
      required:true,
      digits:true,
      maxlength:4
    },
    director_pel:{
      required:true
    },
		fk_id_gen:{
      required:true
		},
    costo_alquiler_pel:{
      required:true,
      digits:true,
      maxlength:3
    }
  },
  messages:{
    titulo_pel:{
      required:"<br>Por favor ingrese el título",
      remote:"Este titulo ya existe"

    },
    duracion_pel:{
      required:"<br>Por favor ingrese la duración",
      digits:"<br>Por favor ingrese solo numeros",
      maxlength:"<br>Por favor ingrese 4 digitos"
    },
    director_pel:{
      required:"<br>Por favor ingrese el director"
    },
		fk_id_gen:{
      required:"<br>Por favor seleccione el genero"
		},
    costo_alquiler_pel:{
      required:"<br>Por favor ingrese el costo del alquiler",
      digits:"<br>Por favor ingrese solo numeros",
      maxlength:"<br>Por favor ingrese 3 digitos"
    }
  },
	errorElement : 'span'
});
</script>
