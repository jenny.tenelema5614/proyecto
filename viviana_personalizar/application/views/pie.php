<!-- Footer section -->
	<footer class="footer-section">
		<div class="container">
			<div class="row">
				<div class="col-xl-6 col-lg-7 order-lg-2">
					<div class="row">
						<div class="col-sm-4">
							<div class="footer-widget">
								<h2>CARTELERA</h2>
								<ul>
									<li><a href="">Próximos Estrenos</a></li>
									<li><a href="">Quiénes Somos</a></li>
									<li><a href="">Donde estamos</a></li>
								</ul>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="footer-widget">
								<h2>Supercines</h2>
								<ul>
									<li><a href="">Término y Condiciones</a></li>
									<li><a href="">Cumpleañps</a></li>

								</ul>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="footer-widget">
								<h2>Tipos de películas</h2>
								<ul>
									<li><a href="">Película Romantica</a></li>
									<li><a href="">Película de Comedia</a></li>
									<li><a href="">Película de Acción</a></li>
									<li><a href="">Caricaturas</a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xl-6 col-lg-5 order-lg-1">
					<img src="<?php echo base_url(); ?>/assets/img/logo2.png" alt="">
					<div class="copyright"><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
 Elaborado por Viviana Tenelema &;<script>document.write(new Date().getFullYear());</script> Estudiante | de la UTC <i class="fa fa-heart-o" aria-hidden="true"></i>  <a href="https://colorlib.com" target="_blank"></a>
					<div class="social-links">
						<a href="https://Instagram.com/supercines"><i class="fa fa-instagram"></i></a>
						<a href="https://www.whatsapp.com/"><i class="fa fa-whatsapp"></i></a>
						<a href="https://www.facebook.com/supercines"><i class="fa fa-facebook"></i></a>
						<a href="https://twitter.com/#!/supercines1"><i class="fa fa-twitter"></i></a>
						<a href="https://www.youtube.com/"><i class="fa fa-youtube"></i></a>
					</div>
				</div>
			</div>
		</div>
	</footer>
	<!-- Footer section end -->

	<!--====== Javascripts & Jquery ======-->
	<!-- <script src="<?php echo base_url(); ?>/assets/js/jquery-3.2.1.min.js"></script> -->
	<script src="<?php echo base_url(); ?>/assets/js/bootstrap.min.js"></script>
	<script src="<?php echo base_url(); ?>/assets/js/jquery.slicknav.min.js"></script>
	<script src="<?php echo base_url(); ?>/assets/js/owl.carousel.min.js"></script>
	<script src="<?php echo base_url(); ?>/assets/js/mixitup.min.js"></script>
	<script src="<?php echo base_url(); ?>/assets/js/main.js"></script>

	</body>
</html>
