<?php

$totalGeneros=0;
$totalAlquileres=0;
$porcentajeGeneros=0;
$porcentajeAlquileres=0;
$total=0;//sumatoria de cliente
if ($generos) {
  $totalGeneros=sizeof($generos->result());
  // code...
}
if ($alquileres) {
    $totalAlquileres=sizeof($alquileres->result());
  // code...
}

$total=$totalGeneros+$totalAlquileres;
if ($total>0) {
  $porcentajeGeneros=$totalGeneros*100/$total;
  $porcentajeAlquileres=$totalAlquileres*100/$total;
  // code...
}
 ?>
<br>
<br>
<br>
<br>
 <br>
<br>
<div class="container">
  <div class="row">
    <div class="col-md-12 text-center" >
      <legend><b> DASHBOARD (TABLERO DE INDICADORES)</b></legend>
    </div>

    </div>
  </div>
<div class="row">
  <div class="col-md-4 text-center" style="padding:50px;">
    <div class="tarjeta">
      <label>ALQUILERES</label>
      <br>
      <div> <?php echo $totalAlquileres?> </div>
    </div>
  </div>

    <div class="col-md-4 text-center" style="padding:50px;">
      <div class="tarjeta">
        <label>GENERO</label>
        <br>
        <div> <?php echo $totalGeneros ?></div>
      </div>

    </div>


</div>
  <br>
<div class="row">
  <div class="col-md-6">
    <canvas id="graficoCircular1" width="300" height="300"></canvas>

  </div>
  <div class="col-md-6">
    <canvas id="graficoBarras1" width="150px" ></canvas>

  </div>
</div>


</div>
</div>
<br>
<br>
<style media="screen">
      .tarjeta{
        border-radius:10px;
        background-color: #dedede;
        padding:30px;
      }

      .tarjeta label{
        font-size:20px;
        color:#338FD0;
      }

      .tarjeta div{
        font-size:50px;
        font-weight:bold;
      }
</style>
<script type="text/javascript">
var graficoCircular1 = new Chart($
("#graficoCircular1"), {
  type: "pie",
  data: {
    labels: ["Alquileres", "Generos"],
    datasets: [{
      label: "Total",
      data: [<?php echo round($porcentajeGeneros,2) ?>, <?php echo round($porcentajeGeneros,2) ?>],
      backgroundColor: [
        "#ff0000",
        "#00ff00"
      ],
      offset: [
        20
      ]
    }]
  },
  options: {
    color: "#0000ff",
    responsive: true,
    maintainAspectRatio: true,
    aspectRatio: 2,
    layout: {
      padding: 20
    },
    rotation: 90,
    plugins: {
      legend: {
        position: "right",
        labels: {
          boxWidth: 25,
          boxHeight: 25,
          font: {
            weight: "bold",
            family: "Noto Sans"
          }
        }
      }
    }
  }
});


var graficoBarras1 = new Chart(
		$("#graficoBarras1"), {
			type: "bar",
			data: {
				labels: ["Alquileres", "Generos"],
				datasets: [{
					label: "Total",
					data: [<?php echo $totalAlquileres; ?>, <?php echo $totalGeneros; ?>],
					backgroundColor:
          ['#DEA61D','#211DDE']
          ,
				}]
			},
			options: {
				plugins: {
					// Quitar Legend
					legend: {
						display: false,
					}
				},
				responsive: true,
				maintainAspectRatio: true,
				scales: {
					yAxes: {
						suggestedMin: "0%",
						suggestedMax: "40%",
						ticks: {
							// forces step size to be 50 units
							stepSize: 20,
							callback: function(value, index, values) {
								return value + '%';
							},
							color: "#000000",
							font: {
								weight: "bold",
								family: "Noto Sans"
							}
						}
					},
					xAxes: {
						ticks: {
							color: "#ff0000",
							font: {
								weight: "bold",
								family: "Noto Sans"
							}
						}
					}
				},
			}
		}
	);

</script>
